FROM python:3.6.11-slim-buster

LABEL maintainer="John Doe <john.doe@gmail.com>"

# create the app user
RUN groupadd --system user && \
	useradd -g user --create-home --shell /bin/bash user

# Linux envs
ENV HOME="/home/user"
ENV APP="${HOME}/app"

# Python envs
ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1

RUN mkdir ${APP}
WORKDIR ${APP}

COPY ./conf/requirements.txt ./conf/

RUN python3 -m pip install --no-cache-dir -r ./conf/requirements.txt

COPY . .

RUN chown -R user:user ${APP} && chmod -R 744 ${APP}
USER user

EXPOSE 8000

CMD [ "/bin/sh", "./conf/backend-start-cmd.sh" ]
